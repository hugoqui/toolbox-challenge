import React, { useState } from 'react'
import Palindrome from "./Views/Palindrome";

function App() {
  const [words, setWords] = useState([])

  const addWord = (word) => {
    setWords(prevWords => {
      return [word, ...prevWords]
    })
  }

  const style = {
    minHeight: "100vh",
    background: "url(https://marieclairemoreau.com/wp-content/uploads/2015/10/globe-wallpaper-1680x1050.jpg)",
    backgroundSize: "cover",
  }

  return (
    <div style={style} >
      <Palindrome words={words} addWord={addWord} />
    </div >
  )
}

export default App;
