import React, { useRef } from 'react'
import WordComponent from './WordComponent'

export default function Palindrome({ words, addWord }) {

    const wordList = words.map((word, i) =>
        <tr key={i}>
            <WordComponent word={word} />
        </tr>
    );

    const str = useRef()

    const postWord = async (e) => {
        e.preventDefault()
        try {
            const url = `http://localhost:3850/iecho?text=${str.current.value}`
            const req = await fetch(url)
            if (req.ok) {
                let res = await req.json()
                res.origin = str.current.value
                console.log(res)
                addWord(res)
            } else {
                alert(await req.text())
            }
            str.current.value = null
        } catch (error) {
            alert(error)
        }
    }

    return (
        <div className="container p-5">

            <h1 className="text-center text-light">Palindrome Check!</h1>
            <hr />

            <form className="row" onSubmit={postWord}>
                <div className="col-md-6 offset-md-2">
                    <input type="text" ref={str} required placeholder="insert text here to get the reverse value" className="form-control m-2" />
                </div>
                <div className="col-md-2">
                    <div className="d-grid gap-1">
                        <button className="btn btn-danger m-2" type="submit" >Send</button>
                    </div>
                </div>
            </form>

            <div className="row mt-5">
                <div className="col-md-8 offset-md-2">
                    <table className="table table-hover shadow">
                        <thead className="table-dark">
                            <tr>
                                <th className="p-3">Original Word</th>
                                <th className="p-3">Reverse Text</th>
                                <th className="p-3">Is a Palindrome?</th>
                            </tr>
                        </thead>
                        <tbody className="bg-light">
                            {wordList}
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    )
}
