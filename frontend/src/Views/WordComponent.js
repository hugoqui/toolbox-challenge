import React from 'react'

import okIcon from '../img/ok.png'
import wrongIcon from '../img/x.png'

export default function WordComponent({ word }) {

    const src = word.palindrome ? okIcon : wrongIcon


    return (
        <>
            <td className="p-2">{word.origin}</td>
            <td className="p-2">{word.text}</td>
            <td className="p-2">
                <img src={src} alt="hq test" width="30" />
            </td>
        </>

    )
}
