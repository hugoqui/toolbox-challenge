## BACKEND
El backend está desarrollado en NodeJS, utilizando express.

**Instalación**
Abra la consola de comandos en el directorio raíz y ejecute 
    
	cd backend 
	npm install
	
**Ejecución**	
	
	npm start
	
**Pruebas unitaras**
    
	npm test
	
	
## FRONT END
El fronend está desarrollado utilizando ReactJs y Bootstrap para el diseño.


**Instalación**
Abra la consola de comandos en el directorio raíz y ejecute 
    
	cd frontend 
	npm install
	
**Ejecución**	
	
	npm run serve
	
