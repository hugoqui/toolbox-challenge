'use strict'

const express = require('express')
const app = express()
const PORT = process.env.PORT || 3850

//cors
const cors = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    next()
}

app.use(cors)
app.use('/', require('./routes/routes'))
app.listen(PORT, () => console.log(`Listening on ${PORT}`))
