const palindrome = require('./palindrome')

exports.checkPalindrome = (req, res) => {
  try {
    const text = req.query.text || req.params.text
    const reverseText = palindrome.check(text.toLowerCase())

    res.status(200).json(reverseText)
  } catch (error) {
    res.status(400).json({ text: 'no text' })
  }
}
