module.exports = {
  check: (text) => {
    let reverseText = ''
    const originText = text.replace(/\s+/g, '').toLowerCase()
    const textArray = originText.split('')

    textArray.reverse()
    textArray.forEach(char => { reverseText += char.toLowerCase() })

    const result = {
      text: reverseText,
      palindrome: reverseText === originText
    }
    return result
  }

}
