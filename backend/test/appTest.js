const assert = require('chai').assert
const palindrome = require('../controllers/palindrome')

describe('Palindrome', () => {
  it('Reverse text should return aloh', () => {
    const result = palindrome.check('hola')
    assert.equal(result.text, 'aloh')
    assert.equal(result.palindrome, false)
  })

  it('Reverse text should return ana', () => {
    const result = palindrome.check('Ana')
    assert.equal(result.text, 'ana')
    assert.equal(result.palindrome, true)
  })

  it('Reverse sentence Ama a la Dama should return false', () => {
    const result = palindrome.check('Ama a la Dama')
    assert.equal(result.text, 'amadalaama')
    assert.equal(result.palindrome, false)
  })

  it('Reverse sentence  Atar a la Rata should return true', () => {
    const result = palindrome.check('Atar a la Rata')
    assert.equal(result.text, 'ataralarata')
    assert.equal(result.palindrome, true)
  })
})
