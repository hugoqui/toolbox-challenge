const express = require('express')
const router = express.Router()
const palindrome = require('../controllers/palindromeController')

router.get('/iecho/:text?', palindrome.checkPalindrome)

module.exports = router
